class Fila{
    constructor(){
        this.items = [];
        this.cont = 0;
        this.menorcont = 0
    }
    add(elemento){
        this.items[this.cont] = elemento;
        this.cont++;
    }
    remove(){
        if(this.estaVazia()){
            return undefined;
        }
        const result = this.items[this.menorcont];
        delete this.items[this.menorcont];
        this.menorcont++;
        return result;
    }
    estaVazia(){
        return this.cont - this.menorcont === 0;
    }
    tamanho(){
        return this.cont - this.menorcont;
    }
    primeiro(){
        if(this.estaVazia()){
            return undefined;
        }
        return this.menorcont;
    }
    limpar(){
        this.items = [];
        this.cont = 0;
        this.menorcont = 0;
    }

    print(){
        //escrever código para imprimir aqui
        //document.getElementById("resposta").innerHTML = pilha.items;
        //limpa a impressão da pilha
        document.getElementById("resposta").innerHTML = "";
        //imprime a Fila
        this.items.forEach(printFila);
    
    }

}//fim classe Fila

function printFila(item, index){
    //1) Criar o elemento
    //2) Criar o texto do elemento
    //3) Adicionar o texto ao elemento
    //4) Adicionar o elemento já com texto ao elemento pai
    let node = document.createElement("div");//1
    node.setAttribute("id", "Fila"); 
    node.setAttribute("name", index);
    if(index == fila.primeiro()){
        node.className = "border border-danger";
    }   
    let text = document.createTextNode(item);//2
    node.appendChild(text);//3
    document.getElementById("resposta").appendChild(node);

}

function addButton(event){
    //1) pega o valor que está na caixa de texto
    //1.1) verificar se a caixa de texto não está vazia
    //2) add esse valor na pilha
    //3) imprime a pilha
    //4) limpar a caixa de texto
    //5) retorna o focus para a caixa de texto
    //6) faz o enter acionar a função add

    let valor = document.getElementById("entrada").value; //1
    if(valor != ""){ //1.1
    fila.add(valor)//2
    fila.print();//3
    document.getElementById("avisos").innerHTML = ""
    } else {
        document.getElementById("avisos").innerHTML = "Não é possível inserir um valor nulo"
    }
    document.getElementById("entrada").value = "";//4
}

// Habilita  a tecla enter
document.addEventListener('keypress', function(e){
    if(e.which == 13){
      addButton();
    }
  }, false);

  

function removeButton(){
    //1) remove o primeiro elemento da fila
    //2) imprime a pilha
    //3) se a pilha já estiver vazia, exibe um aviso
    if(fila.estaVazia() == true){
        fila.limpar()  
    } else {
        fila.remove();
    }
    fila.print();//2
}
const fila = new Fila();
console.log(fila)
fila.print();

