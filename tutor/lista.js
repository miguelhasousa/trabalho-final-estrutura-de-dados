
$(function () {
    $('[data-toggle="popover"]').popover();
});

class Node {
    constructor(element) {
        this.value = element;
        this.next = undefined;
    }
}

class Lista {
    constructor() {
        this.head = undefined;
        this.count = 0;
    }

    // Outros Métodos da classe Lista
    // push() - adicionar um elemento no final da Lista
    push(element) {
        const node = new Node(element);
        let current;

        if (this.head == null) {
            this.head = node;
        } else {
            current = this.head;
            while (current.next != null) {
                current = current.next;
            }

            current.next = node;
        }

        this.count++;
    }

    // getElementAt(position) - retorna um elemento de uma posição especifica da lista
    getElementAt(position) {
        if (position >= 0 && position <= this.count) {
            let node = this.head;
            for (let i = 0; i < position && node != null; i++) {
                node = node.next;
            }
            return node;
        }
        return undefined;
    }

    // insertAt(element, position) - adiciona um elemento em qualquer posição da Lista
    insertAt(element, position) {
        if (position >= 0 && position <= this.count) {
            const node = new Node(element);
            if (position === 0) {
                const current = this.head;
                node.next = current;
                this.head = node;
            } else {
                const previous = this.getElementAt(position - 1);
                const current = previous.next;
                node.next = current;
                previous.next = node;
            }
            this.count++;
            return true;
        }
        return false;
    }

    // removeAt(position) - remove um elemento de uma posição especifica da lista
    removeAt(position) {
        if (position >= 0 && position <= this.count) {
            const current = this.head;
            if (position === 0) {
                this.head = current.next;
            } else {
                const previous = this.getElementAt(position - 1);
                const current = previous.next;
                previous.next = current.next;
            }
            this.count--;
            return true;
        }
        return false;
    }
    // remove() - remove um elemento da Lista
    // indexOf(element) - retorna a posição de um elemento da Lista

    indexOf(element) {
        let current = this.head;

        for (let i = 0; this.count && current != null; i++) {
            if (current.value == element) {
                return i;
            } else {
                current = current.next;
            }
        }
        return -1;
    }

    // Verifica se a lista contem valores repetidos
    naoRepete(){
        for(let i = 0; i < this.count; i++){
            
            for(let j = i + 1 ; j < this.count; j++){

                let val1 = this.getElementAt(i).value;
                let val2 = this.getElementAt(j).value;

                if( val1 == val2 ){
                   return true;
                }           
            }       
        }
        return false;
    }

    // isEmpty() - checar se a lista está vazia
    isEmpty() {
        if (this.count === 0) {
            return true;
        } else {
            return false;
        }
    }
    // size() - retorna o tamanho da lista
    size(){
        return this.count;
    }

    print(){
        document.getElementById("apresentaLista").innerHTML = "";

        let current = this.head;

        for(let i = 0; i < this.count && current != null; i++){
            let atual = current;
            exibeFila(atual.value, i);
            current = current.next;
        }

        // Iniciando Popovers
        $(function () {
            $('[data-toggle="popover"]').popover();
        });
    }

    removeElementValue(element){
        if(this.naoRepete(element) == true){
            let posicao = this.indexOf(element);
            this.removeAt(posicao);
            return true;
        } else {
            return false;
        }
    }

    firstElement(){
        return this.head.value; 
    }

    // Verifica o ultimo elemento
    lastElement(){
        let val;
        for(let i = 0; i < this.count; i++){
            val = this.getElementAt(i).value;  
        }
        return val;
    }

    inverter(element1, element2){
        let val1, val2;
        let val1existe, val2existe;
        let inverte1, inverte2;
        let anterior1, anterior2;
        let sucessor1, sucessor2;

        for(let i = 0; i < this.count; i++){
            val1 = this.getElementAt(i).value;
            if(val1 == element1 ){
                val1existe = true;
                anterior1 = this.getElementAt(val1 - 1).value;
                sucessor1 = this.getElementAt(val1 + 1).value;
            }
        }

        for(let i = 0; i < this.count; i++){
            val2 = this.getElementAt(i).value;
            if(val2 == element2 ){
                val2existe = true;
                anterior2 = this.getElementAt(val2 - 1).value;
                sucessor2 = this.getElementAt(val2 + 1).value;
            }
        }
        if(val1existe != true || val2existe != true){
            return false;
        } else {
            return true;
        }
    }

    clear(){
        this.head = undefined;
        this.count = 0;
    }
}

function cardButton(define){
    let aux = Number(define);
    let next, previous;
    let tamanho = lista.size();
    let tipo = typeof lista.getElementAt(aux).value;
    let valor = lista.getElementAt(aux).value;
    let position = aux + 1;

    if(aux < 0 ){
        previous = "Não há anteriores";

    } else if(aux > 0){
        previous = lista.getElementAt(aux - 1).value;
    }

    if(aux == tamanho - 1){
        next = "Não há próximo";
    } else {
        next = lista.getElementAt(aux + 1).value;
    }

    let info = "Valor = " + valor + ", Posição = " + position + ", Tipo = " + tipo + ", Elemento Anterior = " + previous + ", Próximo Elemento = " + next;

    return info;
}

// Exibindo a Fila
function exibeFila(item, index){

    let node = document.createElement("button");
    
    let texto = document.createTextNode(item);
    node.appendChild(texto);

    let info = cardButton(index);

    node.setAttribute("type", "button");
    node.setAttribute("id", "btn");
    node.className = "btn btn-success";
    node.setAttribute("data-container","body");
    node.setAttribute("data-toggle","popover");
    node.setAttribute("data-placement","top");
    node.setAttribute("data-content", info);
    node.setAttribute("value", item);
    node.setAttribute("index", index);

    let listas = document.getElementById("apresentaLista");
    listas.appendChild(node, listas.childNodes[0]);

    document.getElementById("elemento").focus();
}



// Exercicio 01
function addElemento() {
    let inputElemento = document.getElementById("elemento").value;
    let avisos = document.getElementById("avisos");

    if(inputElemento == ""){
        // Verificando se o inputElemento não está vazio
        avisos.innerHTML = "<div class='alert alert-warning' role='alert'>ERRO! o Campo Elemento deve ser preenchido</div>";
    } else {
        lista.push(inputElemento);
        avisos.innerHTML = "";
        document.getElementById("elemento").value = "";
    }

    lista.print();  
}

// Exercicio 02
function addElementoPos(){
    var tamanho = lista.size();

    let inputElemento = document.getElementById("elemento").value;
    let posicao = document.getElementById("posicao").value;
    let avisos = document.getElementById("avisos");

    if(inputElemento == "" || posicao == ""){
        avisos.innerHTML = "<div class='alert alert-warning' role='alert'>ERRO! os Campos Elemento  e Posição devem ser preenchidos</div>";
    } else if(!isNaN(posicao) == false){
        avisos.innerHTML = "<div class='alert alert-warning' role='alert'>ERRO! o Campo Posição deve ser numérico e inteiro</div>";
        document.getElementById("posicao").focus();
    } else if(posicao > tamanho){
        avisos.innerHTML = "<div class='alert alert-warning' role='alert'>ERRO! o Campo Posição deve ser menor ou igual a "+ tamanho + "</div>";
        document.getElementById("posicao").focus();
    } else {
        lista.insertAt(inputElemento, posicao);
        avisos.innerHTML = "";
        document.getElementById("elemento").value = "";
        document.getElementById("posicao").value = "";
    }
    lista.print();
}

// Exercicio 03
function removeElementoValue(){
    let inputElemento = document.getElementById("remover").value;
    let avisos = document.getElementById("avisos");

    if(inputElemento == ""){
        avisos.innerHTML = "<div class='alert alert-warning' role='alert'>ERRO! o Campo Remover deve ser preenchido</div>";
        document.getElementById("remover").focus();
    } else {  
        removido = lista.removeElementValue(inputElemento);
        if( removido == true ){
            avisos.innerHTML = "<div class='alert alert-success' role='alert'>Elemento removido com sucesso!</div>";
            document.getElementById("remover").value = "";
        } else {
            avisos.innerHTML = "<div class='alert alert-warning' role='alert'>Elemento Não existe</div>";
        }

        lista.print();
    }
}

function removePosition(){
    let posicao = document.getElementById("posicao").value;
    let avisos = document.getElementById("avisos");
    let tamanho = lista.size();

    tamanho = tamanho - 1;

    if(posicao == ""){
        document.getElementById("inverter1").focus();
        avisos.innerHTML = "<div class='alert alert-warning' role='alert'>ERRO! o Campo Posição deve ser preenchido</div>";
    } else if(!isNaN(posicao) == false){
        document.getElementById("posicao").focus();
        avisos.innerHTML = "<div class='alert alert-warning' role='alert'>ERRO! o Campo Posição deve ser numérico e inteiro</div>";
    } else if(posicao > tamanho){
        avisos.innerHTML = "<div class='alert alert-warning' role='alert'>ERRO! o Campo Posição deve ser menor ou igual a "  + tamanho + "</div>";
        document.getElementById("posicao").focus();
    } else {
        lista.removeAt(posicao);
        avisos.innerHTML = "<div class='alert alert-success' role='alert'>Elemento removido com sucesso!</div>";
        document.getElementById("posicao").value = "";
        avisos.innerHTML = "";
    } 
    lista.print();
}


function verificaVazio(){
    let tamanho = lista.isEmpty();
    let avisos = document.getElementById("avisos");

    if(tamanho == true){
        avisos.innerHTML = "<div class='alert alert-warning' role='alert'>A Lista ESTÁ Vazia!</div>";
    } else {
        avisos.innerHTML = "<div class='alert alert-success' role='alert'>A Lista não está vazia</div>";
    }
}

function primeiroElemento(){
    let tamanho = lista.size();
    let primeiro = lista.firstElement();
    let avisos = document.getElementById("avisos");

    if(tamanho < 1){
        avisos.innerHTML = "<div class='alert alert-warning' role='alert'>A Lista ESTÁ Vazia!</div>";
    } else {
        avisos.innerHTML = "";
        avisos.innerHTML = "<div class='alert alert-success' role='alert'>O primeiro elemento é " + primeiro + "</div>";
    }
}

function ultimoElemento(){
    let tamanho = lista.size();
    let ultimo = lista.lastElement();
    let avisos = document.getElementById("avisos");

    if(tamanho < 1){
        avisos.innerHTML = "<div class='alert alert-warning' role='alert'>A Lista ESTÁ Vazia!</div>";
        
    } else {
        avisos.innerHTML = "<div class='alert alert-success' role='alert'>O ultimo elemento é " + ultimo + "</div>";
    }
}

function reverte(){
    let elemento1 = document.getElementById("inverter1").value;
    let elemento2 = document.getElementById("inverter2").value;
    let avisos = document.getElementById("avisos");
    
    if(elemento1 == "" || elemento2 == ""){
        document.getElementById("inverter1").focus();
        avisos.innerHTML = "<div class='alert alert-warning' role='alert'>Os Campos Inverter Elementos devem ser preenchidos!</div>";
    } else {
        let teste = lista.inverter(elemento1, elemento2); 
        avisos.innerHTML = "Os Valores " + elemento1 + " e " + elemento2 + " " + teste + " Existe";
        document.getElementById("inverter1").value = "";
        document.getElementById("inverter2").value = "";
    }
}

function verficaRepetidos(){
    let repete = lista.naoRepete();
    let avisos = document.getElementById("avisos");

    if(lista.naoRepete()){
        avisos.innerHTML = "<div class='alert alert-warning' role='alert'>Há Elementos repetidos na Lista!</div>";
    } else {
        avisos.innerHTML = "<div class='alert alert-success' role='alert'>Não Há Elementos repetidos na Lista!</div>";
    }
}


function limparLista(){
    let avisos = document.getElementById("avisos");
    let tamanho = lista.size();

    if(tamanho < 1){
        avisos.innerHTML = "<div class='alert alert-warning' role='alert'>A Lista já esta Vazia</div>";
    } else {
        lista.clear();
        avisos.innerHTML = "<div class='alert alert-success' role='alert'>A lista foi limpada!</div>";
        lista.print();
    }
}

const lista = new Lista();

lista.push(15);
lista.push(10);
lista.push(5);
lista.push(84);
lista.print();
