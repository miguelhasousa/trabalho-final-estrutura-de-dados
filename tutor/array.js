var A = ["Se você traçar metas absurdamente altas e falhar, seu fracasso será muito melhor que o sucesso de todos",
         "Para de perseguir o dinheiro e comece a perseguir o sucesso",
         "Não é o mais forte que sobrevive, nem o mais inteligente. Quem sobrevive é o mais disposto à mudança",
         "A melhor vingança é um sucesso estrondoso",
         "Um homem de sucesso é aquele que cria uma parede com os tijolos que jogaram nele",
         "O grande segredo de uma boa vida é encontrar qual é o seu destino. E realizá-lo",
         "Se você está atravessando um inferno, continue atravessando",
         "As pessoas me perguntam qual é o papel que mais gostei de interpretar. Eu sempre respondo: o próximo",
         "Descobri que, quanto mais eu trabalho, mais sorte eu pareço ter",
         "O ponto de partida de qualquer conquista é o desejo",
         "O sucesso é a soma de pequenos esforços repetidos dia após dia",
         "Todo progresso acontece fora da zona de conforto",
         "Coragem é a resistência e o domínio do medo, não a ausência dele",
         "Só evite fazer algo hoje se você quiser morrer e deixar assuntos inacabados",
         "O único lugar em que o sucesso vem antes do trabalho é no dicionário",
         "Sempre que você se encontrar do lado da maioria, é hora de parar e refletir",
         "Quanto maior o artista, maior a dúvida. Confiança grande demais é algo destinados aos menos talentosos como um prêmio de consolação",
         "Tenha em mente que o seu desejo em atingir o sucesso é mais importante que qualquer coisa",
         "Fique contente em agir. Deixe a fala para os outros",
         "Para conquistar o sucesso, você precisa aceitar todos os desafios que vierem na sua frente. Você não pode apenas aceitar os que você preferir",
         "O guerreiro de sucesso é um homem médio, mas com um foco apurado como um raio laser",
         "A lógica pode levar de um ponto A a um ponto B. A imaginação pode levar a qualquer lugar",
         "Os empreendedores falham, em média, 3,8 vezes antes do sucesso final. O que separa os bem-sucedidos dos outros é a persistência",
         "Comece de onde você está. Use o que você tiver. Faça o que você puder",
         "As pessoas me perguntam qual é o papel que mais gostei de interpretar. Eu sempre respondo: o próximo",
         "Todos os seus sonhos podem se tornar realidade se você tem coragem para persegui-los",
         "Ter sucesso é falhar repetidamente, mas sem perder o entusiasmo",
         "Sucesso? Eu não sei o que isso significa. Eu sou feliz. A definição de sucesso varia de pessoa para pessoa Para mim, sucesso é paz anterior",
         "Oportunidades não surgem. É você que as cria",
         "Não tente ser uma pessoa de sucesso. Em vez disso, seja uma pessoa de valor"];

function geraRandomInt() {
    min = Math.ceil(0);
    max = Math.floor(30);
    return Math.floor(Math.random() * (max - min)) + min;
}

function geraRandomClick() {
    min = Math.ceil(0);
    max = Math.floor(30);
    return document.getElementById("exibefrase").innerHTML = (A[geraRandomInt()]);
}

document.getElementById("exibefrase").innerHTML = (A[geraRandomInt()]);