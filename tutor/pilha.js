class Pilha{
    constructor(){
        this.items = [];
    }
    push(element){
        this.items.push(element);
    }
    pop(){
        this.items.pop();
    }  
    isEmpty(){
        if (this.items.length === 0 ){
            return true;
        }
        return false;
    }
    peek(){
        return this.items[this.items.length - 1];
    }
    size(){
        return this.items.length;
    }
    print(){
        document.getElementById("resposta").innerHTML = "";
        this.items.forEach(printPilha);
    }
}

function printPilha(item, index){
    let node = document.createElement("div");
    let text = document.createTextNode(item);
    node.appendChild(text);
    let list = document.getElementById("resposta");
    list.insertBefore(node, list.childNodes[0]);
}

function addButton(event){
    let valor = document.getElementById("entrada").value; 
    if(valor != ""){ 
        pilha.push(valor);
        pilha.print();
        document.getElementById("avisos").innerHTML = "";
    } else {
        document.getElementById("avisos").innerHTML = "Não é possível inserir um valor nulo";
    }
    document.getElementById("entrada").value = "";
    document.getElementById("pop").disabled = false;
}  

function removeButton(){
    if(pilha.isEmpty() == true){
        document.getElementById("pop").disabled = true;
        pilha.pop();
        pilha.print();
        document.getElementById("avisos").innerHTML = "A pilha já está vazia";
    } else {
        pilha.pop();
        pilha.print();
    }
   
}

function ultimoButton(){
    if(pilha.isEmpty() == false){
        document.getElementById("avisos").innerHTML = "O ultimo elemento da pilha é " + pilha.peek();
    } else {
        document.getElementById("avisos").innerHTML = "A pilha está vazia";
    }
}
const pilha = new Pilha();
pilha.print();